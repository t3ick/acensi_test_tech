Test technique

Pré-requis :
Stack technique : PHP5/7, HTML5, Css3, Mysql, JavaScript, doctrine, git
Durée de test: 2H-3H
Internet/ documentation : Autoriser
NB: il faut pousser votre travail sur votre GIT personnel et l&#39;envoyer par mail sur
mejdouline.ouardigui@acensi.fr
Niveau 1:
1. Installer le framework Symfony (En utilisant Composer)
2. Créer une première classe Student (Entity) qui contient les champs
   (FirstName 25 caractère / LastName 25 caractère / NumEtud 10 chiffre )
3. Générer les formulaires pour cette classe
4. Créer les CRUD :
    Lister tous les enregistrements (Formulaire d&#39;affichages)
    Créer un enregistrement (Formulaire d&#39;ajout)
    Modifier un enregistrement existant (Formulaire de modification)
    Supprimer un enregistrement existant (Bouton de suppression )
   Niveau 2:
    Intégrer Bootstrap à votre projet
    Adapter Bootstrap à vos formulaires
   Niveau 3:
1. Créer une deuxième classe Department (Entity) qui contient les champs
   (Name 25 caractère / Capacity 10 chiffre )
2. Créer la relation entre la classe Departement et Student (OneToMany)
   Niveau 4:
1. Créer un ApiRest en utilisant FosRestBundle ou Api-Platform pour lister tous les
   étudiants inscrits dans un département
2. Créer un Api documentation